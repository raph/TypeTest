package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe appell�e lors du clique sur le menu "enregistrer"
 * @author Rapha�l ALVES
 *
 */
public class SaveListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
	main.Main.app.save();
    }

}
