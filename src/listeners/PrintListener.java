package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe appell�e lors du clique sur le menu "Imprimer"
 * @author Rapha�l ALVES
 *
 */
public class PrintListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
	main.Main.app.print();
    }
}
