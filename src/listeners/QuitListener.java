package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe appell�e lors du clique sur le menu "quitter"
 * @author Rapha�l ALVES
 *
 */
public class QuitListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
	System.exit(0);
    }

}
