package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe appell�e lors du click sur le menu "A propos"
 * @author Rapha�l ALVES
 *
 */
public class AboutListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
	main.Main.app.showAbout();
    }

}
