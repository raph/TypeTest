package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe appell�e lorsque l'on clique sur "ok" apr�s avoir rensign� les infos sur le candidat
 * @author Rapha�l ALVES
 *
 */
public class FirstPanelListener implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
	main.Main.app.next();
    }

}
