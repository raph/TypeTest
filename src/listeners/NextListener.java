package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

import main.Main;
import ui.SecondPanel;

/**
 * Classe appell�e � chaque fois que l'on clique sur le bouton "suivant"
 * @author Rapha�l ALVES
 *
 */
public class NextListener implements ActionListener {
    
    private SecondPanel panel;
    private boolean started;
    
    public NextListener(SecondPanel panel) {
	this.panel = panel;
	started = false;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
	if (! started) {
	    // On change le texte du bouton de "d�marrer" � "suivant"
	    ((JButton) arg0.getSource()).setText(data.Constants.NEXT_BUTTON);
	    started = true;
	    panel.enableAll();
	    // On start le timer
	    panel.getTimer().start();
	}
	else {
	    Main.app.compareResponses();
	    Main.app.next();
	}
	
    }

}
