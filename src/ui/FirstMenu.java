package ui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import listeners.AboutListener;
import listeners.QuitListener;

import static data.Constants.*;

/**
 * JMenuBar affich� sur la page d'acceuil du logiciel
 * @author Rapha�l ALVES
 *
 */
public class FirstMenu extends JMenuBar {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private JMenu file, about;
    
    public FirstMenu() {
	file = new JMenu(FILE_MENU);
	about = new JMenu(HELP_MENU);
	
	JMenuItem quit = new JMenuItem(QUIT_MENU_ITEM);
	quit.addActionListener(new QuitListener());
	file.add(quit);
	JMenuItem aboutDialog = new JMenuItem(ABOUT_MENU_ITEM);
	aboutDialog.addActionListener(new AboutListener());
	about.add(aboutDialog);
	
	add(file);
	add(about);
    }

}
