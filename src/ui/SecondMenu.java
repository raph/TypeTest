package ui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import listeners.AboutListener;
import listeners.PrintListener;
import listeners.QuitListener;
import listeners.SaveListener;

import static data.Constants.*;

/**
 * Second menu a afficher dans la frame (avec "enregistrer", "imprimer", ect...)
 * @author Rapha�l ALVES
 *
 */
public class SecondMenu extends JMenuBar {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public SecondMenu() {
	JMenu file = new JMenu(FILE_MENU);
	JMenu about = new JMenu(HELP_MENU);

	JMenuItem save = new JMenuItem(SAVE_MENU_ITEM);
	save.addActionListener(new SaveListener());
	file.add(save);
	JMenuItem print = new JMenuItem(PRINT_MENU_ITEM);
	print.addActionListener(new PrintListener());
	file.add(print);
	JMenuItem quit = new JMenuItem(QUIT_MENU_ITEM);
	quit.addActionListener(new QuitListener());
	file.add(quit);
	
	JMenuItem aboutdialog = new JMenuItem(ABOUT_MENU_ITEM);
	aboutdialog.addActionListener(new AboutListener());
	about.add(aboutdialog);
	
	add(file);
	add(about);
    }

}
