package ui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;

import static data.Constants.*;
import java.awt.Font;

/**
 * Dans ce panel on trouvera les infos sur le candidat, les scores, et le boutton "suivant"
 * @author Rapha�l ALVES
 *
 */
public class ControlsPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private JLabel lastname;
    private JLabel firstname;
    
    private JLabel timer;
    
    private JLabel correctFieldsNumberLabel;
    private JLabel incorrectFieldsNumberLabel;
    
    private JLabel incorrectsNumber;
    private JLabel correctsNumber;
    
    private JButton nextButton;
    
    private JLabel grossspeed;
    private JLabel realspeed;
    private JLabel titleLabel;
    
    public ControlsPanel() {
	setBackground(Color.WHITE);
	setPreferredSize(new Dimension(570, 204));
	setSize(570, 280);
	
	correctFieldsNumberLabel = new JLabel(CORRECTFIELDS_LABEL);
	correctFieldsNumberLabel.setBounds(10, 11, 182, 14);
	
	correctsNumber = new JLabel("0");
	correctsNumber.setBounds(202, 11, 31, 14);
	correctsNumber.setFont(new Font("Tahoma", Font.BOLD, 11));
	correctsNumber.setForeground(new Color(0, 128, 0));
	
	incorrectFieldsNumberLabel = new JLabel(ERRORSNUMBER_LABEL);
	incorrectFieldsNumberLabel.setBounds(10, 40, 182, 14);
	
	incorrectsNumber = new JLabel("0");
	incorrectsNumber.setBounds(202, 40, 31, 14);
	incorrectsNumber.setFont(new Font("Tahoma", Font.BOLD, 11));
	incorrectsNumber.setForeground(new Color(255, 0, 0));
	
	JLabel grossTypeSpeedLabel = new JLabel(GROSSTYPINGSPEED_LABEL);
	grossTypeSpeedLabel.setBounds(10, 72, 182, 14);
	
	grossspeed = new JLabel("0");
	grossspeed.setBounds(202, 72, 92, 14);
	
	JLabel realTypeSpeedLabel = new JLabel(REALTYPINGSPEED_LABEL);
	realTypeSpeedLabel.setBounds(10, 104, 182, 14);
	
	realspeed = new JLabel("0");
	realspeed.setBounds(202, 104, 92, 14);
	
	JLabel timerLabel = new JLabel(REMAININGTIME_LABEL);
	timerLabel.setBounds(10, 135, 182, 14);
	
	timer = new JLabel("0" + TIME / 60 + " : 00");
	timer.setBounds(202, 135, 49, 14);
	
	nextButton = new JButton(START_LABEL);
	nextButton.setBounds(10, 160, 550, 40);
	
	JLabel lastnameLabel = new JLabel(LASTNAME_LABEL);
	lastnameLabel.setBounds(304, 11, 31, 14);
	
	JLabel firstnameLabel = new JLabel(FIRSTNAME_LABEL);
	firstnameLabel.setBounds(304, 43, 61, 14);
	
	lastname = new JLabel();
	lastname.setBounds(366, 11, 79, 14);
	
	firstname = new JLabel();
	firstname.setBounds(366, 43, 92, 14);
	
	titleLabel = new JLabel(MAIN_TITLE);
	titleLabel.setBounds(324, 95, 149, 25);
	titleLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
	setLayout(null);
	add(timerLabel);
	add(realTypeSpeedLabel);
	add(grossTypeSpeedLabel);
	add(incorrectFieldsNumberLabel);
	add(correctFieldsNumberLabel);
	add(timer);
	add(grossspeed);
	add(realspeed);
	add(correctsNumber);
	add(incorrectsNumber);
	add(titleLabel);
	add(firstnameLabel);
	add(lastnameLabel);
	add(lastname);
	add(firstname);
	add(nextButton);
    }
    
    public void setFirstname(String firstname) {
	this.firstname.setText(firstname);
    }
    
    public void setLastname(String lastname) {
	this.lastname.setText(lastname);
    }
    
    public JButton getNextButton() {
	return nextButton;
    }
    
    public void setTime(int minutes, int seconds) {
	if (seconds == 60)
	    timer.setText("0" + (minutes + 1) + " : 00");
	else if (seconds > 9)
	    timer.setText("0" + minutes + " : " + seconds);
	else
	    timer.setText("0" + minutes + " : 0" + seconds);
	
	if (minutes == 0 && seconds < 10)
	    timer.setForeground(Color.RED);
    }
    
    public void setCorrectsNumber(int n) {
	correctsNumber.setText("" + n);
    }
    
    public void setIncorrectNumber(int n) {
	incorrectsNumber.setText("" + n);
    }
    
    public void disable() {
	nextButton.setEnabled(false);
    }
    
    public void setGrossspeed(double speed) {
	grossspeed.setText(speed + SPEED_LABEL);
    }
    
    public void setRealspeed(double speed) {
	realspeed.setText(speed + SPEED_LABEL);
    }
}
