package ui;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.Color;
import java.awt.Component;

import listeners.FirstPanelListener;
import static data.Constants.*;

/**
 * Premier panel, l� o� l'on doit renseigner les infos sur le candidat
 * @author Rapha�l ALVES
 *
 */
public class FirstPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JTextField lastnameTextField;
    private JTextField firstnameTextField;

    /**
     * Create the panel.
     */
    public FirstPanel() {
    	setBackground(Color.WHITE);
    	setLayout(null);
    	
    	JLabel titleLabel = new JLabel(MAIN_TITLE);
    	titleLabel.setBounds(104, 31, 141, 31);
    	titleLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
    	titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    	add(titleLabel);
    	
    	JButton validateButton = new JButton(VALIDATE_BUTTON);
    	validateButton.setBounds(104, 229, 141, 38);
    	validateButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    	add(validateButton);
    	
    	validateButton.addActionListener(new FirstPanelListener());
    	
    	JLabel lastnameLabel = new JLabel(LASTNAME_LABEL);
    	lastnameLabel.setBounds(26, 114, 31, 14);
    	add(lastnameLabel);
    	
    	lastnameTextField = new JTextField();
    	lastnameTextField.setBounds(130, 111, 184, 20);
    	add(lastnameTextField);
    	lastnameTextField.setColumns(10);
    	
    	JLabel firstnameLabel = new JLabel(FIRSTNAME_LABEL);
    	firstnameLabel.setBounds(26, 166, 46, 14);
    	add(firstnameLabel);
    	
    	firstnameTextField = new JTextField();
    	firstnameTextField.setBounds(130, 163, 184, 20);
    	add(firstnameTextField);
    	firstnameTextField.setColumns(10);
    }
    
    public String getLastname() {
	return lastnameTextField.getText();
    }
    
    public String getFirstname() {
	return firstnameTextField.getText();
    }
}