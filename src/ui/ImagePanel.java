package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;

/**
 * Panel ou sera affich�e l'image du formulaire manuscrit
 * @author Rapha�l ALVES
 *
 */
public class ImagePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private BufferedImage image;
    
    public ImagePanel() {
	setBackground(Color.WHITE);
    }
    
    public void setBackground(BufferedImage image) {
	this.image = Scalr.resize(image, Method.ULTRA_QUALITY, 1500);
	repaint();
    }
    
    public void paintComponent(Graphics g) {
	super.paintComponent(g);
	g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }

}
