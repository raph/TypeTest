package ui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;

import javax.swing.JRadioButton;

import static data.Constants.*;

/**
 * Formulaire que doivent remplir les candidats
 * @author Rapha�l ALVES
 *
 */
public class FormPanel extends JPanel {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private JTextField lastNameTextField;
    private JTextField spousesNameTextField;
    private JTextField firstnameTextField;
    private JTextField addressTextField;
    private JTextField postalCodeTextField;
    private JTextField cityTextField;
    private JTextField mobilePhoneTextField;
    private JTextField housePhoneTextField;
    private JTextField birthPlaceTextField;
    private JTextField departmentTextField;
    private JTextField nationalityTextField;
    private JTextField countryTextField;
    private JTextField birthdateTextField;
    private JTextField socialSecurityTextField;
    private JTextField affiliateCashTextField;
    private JTextField otherTextField;
    private JTextField searchedEmploymentsTextField;
    
    private JTextField[] textfields;
    
    private JRadioButton aloneRadioButton;
    private JRadioButton marriedRadioButton;
    private JRadioButton divorcedRadioButton;
    private JRadioButton civilUnionRadioButton;
    private JRadioButton concubineRadioButton;
    private JRadioButton widowRadioButton;
    private JRadioButton generalSchemeCityRadioButton;
    private JRadioButton msaRadioButton;
    private JRadioButton otherRadioButton;
    private JRadioButton byCheckRadioButton;
    private JRadioButton byTransferRadioButton;
    private JRadioButton executiveRadioButton;
    private JRadioButton nonExecutiveRadioButton;
    private JRadioButton noMatterRadioButton;
    private JRadioButton outsideQualificationYesRadioButton;
    private JRadioButton outsideQualificationNoRadioButton;
    private JRadioButton lessQualifiedYesRadioButton;
    private JRadioButton lessQualifiedNoRadioButton;
    
    private JCheckBox allCheckBox;
    private JCheckBox interimCheckBox;
    private JCheckBox cdiCheckBox;
    private JCheckBox cddCheckBox;
    
    private JToggleButton[] toggleButtons;
    
    private ButtonGroup searchedStatus;
    private ButtonGroup outsideQualification;
    private ButtonGroup lessQualified;
    private ButtonGroup salaryModalities;
    private ButtonGroup cashAffiliate;
    private ButtonGroup familyStatus;
    
    public FormPanel() {
	setLayout(null);
	setBackground(Color.WHITE);
		
	JPanel panel = new JPanel();
	panel.setBounds(10, 11, 541, 526);
	add(panel);
	
	JLabel yourIDLabel = new JLabel(ID_LABEL);
	yourIDLabel.setBounds(223, 11, 95, 17);
	yourIDLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	
	JLabel lastnameLabel = new JLabel(LASTNAME_LABEL);
	lastnameLabel.setBounds(10, 42, 112, 14);
		
	lastNameTextField = new JTextField();
	lastNameTextField.setBounds(132, 39, 86, 20);
	lastNameTextField.setColumns(10);
	
	JLabel spousesNameLabel = new JLabel(SPOUSESNAME_LABEL);
	spousesNameLabel.setBounds(10, 71, 112, 14);
		
	spousesNameTextField = new JTextField();
	spousesNameTextField.setBounds(132, 68, 86, 20);
	spousesNameTextField.setColumns(10);
	panel.setLayout(null);
	panel.add(yourIDLabel);
	panel.add(lastnameLabel);
	panel.add(lastNameTextField);
	panel.add(spousesNameLabel);
	panel.add(spousesNameTextField);
	
	JLabel firstnameLabel = new JLabel(FIRSTNAME_LABEL);
	firstnameLabel.setBounds(10, 99, 112, 14);
	panel.add(firstnameLabel);
		
	firstnameTextField = new JTextField();
	firstnameTextField.setBounds(132, 96, 86, 20);
	panel.add(firstnameTextField);
	firstnameTextField.setColumns(10);
	
	JLabel addressLabel = new JLabel(ADDRESS_LABEL);
	addressLabel.setBounds(10, 127, 112, 14);
	panel.add(addressLabel);
	
	addressTextField = new JTextField();
	addressTextField.setBounds(132, 124, 86, 20);
	panel.add(addressTextField);
	addressTextField.setColumns(10);
	
	JLabel postalCodeLabel = new JLabel(POSTALCODE_LABEL);
	postalCodeLabel.setBounds(10, 155, 112, 14);
	panel.add(postalCodeLabel);
	
	postalCodeTextField = new JTextField();
	postalCodeTextField.setBounds(132, 152, 86, 20);
	panel.add(postalCodeTextField);
	postalCodeTextField.setColumns(10);
	
	JLabel cityLabel = new JLabel(CITY_LABEL);
	cityLabel.setBounds(228, 155, 126, 14);
	panel.add(cityLabel);
	
	cityTextField = new JTextField();
	cityTextField.setBounds(364, 152, 86, 20);
	panel.add(cityTextField);
	cityTextField.setColumns(10);
	
	JLabel mobilePhoneLabel = new JLabel(PHONE_MOBILE_LABEL);
	mobilePhoneLabel.setBounds(10, 183, 112, 14);
	panel.add(mobilePhoneLabel);
	
	mobilePhoneTextField = new JTextField();
	mobilePhoneTextField.setBounds(132, 180, 86, 20);
	panel.add(mobilePhoneTextField);
	mobilePhoneTextField.setColumns(10);
	
	JLabel housePhonelabel = new JLabel(HOME_LABEL);
	housePhonelabel.setBounds(228, 183, 126, 14);
	panel.add(housePhonelabel);
	
	housePhoneTextField = new JTextField();
	housePhoneTextField.setBounds(364, 180, 86, 20);
	panel.add(housePhoneTextField);
	housePhoneTextField.setColumns(10);
	
	JLabel birthplaceLabel = new JLabel(BIRTHPLACE_LABEL);
	birthplaceLabel.setBounds(10, 214, 112, 14);
	panel.add(birthplaceLabel);
	
	birthPlaceTextField = new JTextField();
	birthPlaceTextField.setBounds(132, 211, 86, 20);
	panel.add(birthPlaceTextField);
	birthPlaceTextField.setColumns(10);
	
	JLabel departmentLabel = new JLabel(DEPARTMENT_LABEL);
	departmentLabel.setBounds(228, 214, 126, 14);
	panel.add(departmentLabel);
	
	departmentTextField = new JTextField();
	departmentTextField.setBounds(364, 211, 86, 20);
	panel.add(departmentTextField);
	departmentTextField.setColumns(10);
	
	JLabel nationalityLabel = new JLabel(NATIONALITY_LABEL);
	nationalityLabel.setBounds(10, 245, 112, 14);
	panel.add(nationalityLabel);
	
	nationalityTextField = new JTextField();
	nationalityTextField.setBounds(132, 242, 86, 20);
	panel.add(nationalityTextField);
	nationalityTextField.setColumns(10);
	
	JLabel countryLabel = new JLabel(COUNTRY_LABEL);
	countryLabel.setBounds(228, 245, 126, 14);
	panel.add(countryLabel);
	
	countryTextField = new JTextField();
	countryTextField.setBounds(364, 242, 86, 20);
	panel.add(countryTextField);
	countryTextField.setColumns(10);
	
	JLabel birthdateLabel = new JLabel(BIRTHDATE_LABEL);
	birthdateLabel.setBounds(10, 276, 112, 14);
	panel.add(birthdateLabel);
	
	birthdateTextField = new JTextField();
	birthdateTextField.setBounds(132, 273, 86, 20);
	panel.add(birthdateTextField);
	birthdateTextField.setColumns(10);
	
	JLabel familySituationLabel = new JLabel(FAMILYSTATUS_LABEL);
	familySituationLabel.setBounds(10, 305, 126, 14);
	panel.add(familySituationLabel);
	
	aloneRadioButton = new JRadioButton(SINGLE_LABEL);
	aloneRadioButton.setBounds(142, 301, 86, 23);
	panel.add(aloneRadioButton);
	
	marriedRadioButton = new JRadioButton(MARRIED_LABEL);
	marriedRadioButton.setBounds(238, 301, 75, 23);
	panel.add(marriedRadioButton);
	
	divorcedRadioButton = new JRadioButton(DIVORCED_LABEL);
	divorcedRadioButton.setBounds(333, 301, 109, 23);
	panel.add(divorcedRadioButton);
	
	civilUnionRadioButton = new JRadioButton(CIVILUNION_LABEL);
	civilUnionRadioButton.setBounds(143, 327, 85, 23);
	panel.add(civilUnionRadioButton);
	
	concubineRadioButton = new JRadioButton(COHABITANT_LABEL);
	concubineRadioButton.setBounds(238, 327, 93, 23);
	panel.add(concubineRadioButton);
	
	widowRadioButton = new JRadioButton(WIDOWER_LABEL);
	widowRadioButton.setBounds(333, 327, 80, 23);
	panel.add(widowRadioButton);
	
	JLabel socialSecurityLabel = new JLabel(SOCIALSECURITYNUMBER_LABEL);
	socialSecurityLabel.setBounds(10, 356, 170, 14);
	panel.add(socialSecurityLabel);
	
	socialSecurityTextField = new JTextField();
	socialSecurityTextField.setBounds(190, 353, 141, 20);
	panel.add(socialSecurityTextField);
	socialSecurityTextField.setColumns(10);
	
	JLabel affiliateCashLabel = new JLabel(CASHAFFILIATE_LABEL);
	affiliateCashLabel.setBounds(10, 381, 170, 14);
	panel.add(affiliateCashLabel);
	
	generalSchemeCityRadioButton = new JRadioButton(GENERALSCHEMECITY_LABEL);
	generalSchemeCityRadioButton.setBounds(183, 377, 171, 23);
	panel.add(generalSchemeCityRadioButton);
	
	affiliateCashTextField = new JTextField();
	affiliateCashTextField.setBounds(364, 378, 86, 20);
	panel.add(affiliateCashTextField);
	affiliateCashTextField.setColumns(10);
	
	msaRadioButton = new JRadioButton(MSA_LABEL);
	msaRadioButton.setBounds(183, 403, 109, 23);
	panel.add(msaRadioButton);
	
	otherRadioButton = new JRadioButton(OTHER_LABEL);
	otherRadioButton.setBounds(183, 429, 109, 23);
	panel.add(otherRadioButton);
	
	otherTextField = new JTextField();
	otherTextField.setBounds(364, 430, 86, 20);
	panel.add(otherTextField);
	otherTextField.setColumns(10);
		
	JLabel cashModalitiesLabel = new JLabel(SALARYMODALITIES_LABEL);
	cashModalitiesLabel.setBounds(10, 461, 231, 14);
	panel.add(cashModalitiesLabel);
	
	byCheckRadioButton = new JRadioButton(BYCHECK_LABEL);
	byCheckRadioButton.setBounds(245, 457, 109, 23);
	panel.add(byCheckRadioButton);
	
	byTransferRadioButton = new JRadioButton(BYTRANSFER_LABEL);
	byTransferRadioButton.setBounds(364, 457, 109, 23);
	panel.add(byTransferRadioButton);
	
	JPanel panel_1 = new JPanel();
	panel_1.setBounds(10, 548, 541, 340);
	add(panel_1);
	
	JLabel yourSearchLabel = new JLabel(YOURSEARCH_LABEL);
	yourSearchLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
	
	JLabel searchedEmploymentsLabel = new JLabel(JOBSSEARCHED_LABEL);
	
	searchedEmploymentsTextField = new JTextField();
	searchedEmploymentsTextField.setColumns(10);
	
	JLabel contractsTypeLabel = new JLabel(SEARCHEDCONTRACTS_LABEL);
	
	interimCheckBox = new JCheckBox(INTERIM_LABEL);
	
	cdiCheckBox = new JCheckBox(CDI_LABEL);
	
	cddCheckBox = new JCheckBox(CDD_LABEL);
	
	JLabel searchedStatusLabel = new JLabel(SEARCHEDSTATUS_LABEL);
	
	executiveRadioButton = new JRadioButton(EXECUTIVE_LABEL);
	
	nonExecutiveRadioButton = new JRadioButton(NONEXECUTIVE_LABEL);
	
	noMatterRadioButton = new JRadioButton(INDIFFERENT_LABEL);
	
	JLabel employmentsAcceptedLabel = new JLabel(ACCEPTEDJOBS_LABEL);
	
	JLabel yesLabel = new JLabel(YES_LABEL);
	
	JLabel noLabel = new JLabel(NO_LABEL);
	
	JLabel outsideQualificationLabel = new JLabel(OUTSIDEQUALIFICATION_LABEL);
	
	outsideQualificationYesRadioButton = new JRadioButton();
	
	outsideQualificationNoRadioButton = new JRadioButton();
	
	JLabel lblDunNiveauDe = new JLabel(LESSQUALIFIEDLEVEL_LABEL);
	
	lessQualifiedYesRadioButton = new JRadioButton();
	
	lessQualifiedNoRadioButton = new JRadioButton();
	
	allCheckBox = new JCheckBox(ALLCONTRACTS_LABEL);
	GroupLayout gl_panel_1 = new GroupLayout(panel_1);
	gl_panel_1.setHorizontalGroup(
		gl_panel_1.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_1.createSequentialGroup()
				.addGap(10)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_1.createSequentialGroup()
						.addComponent(searchedEmploymentsLabel, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(searchedEmploymentsTextField, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_1.createSequentialGroup()
						.addComponent(contractsTypeLabel, GroupLayout.PREFERRED_SIZE, 188, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
							.addComponent(cdiCheckBox, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(interimCheckBox, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
										.addGap(29)
										.addComponent(searchedStatusLabel, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
									.addComponent(allCheckBox, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
									.addComponent(cddCheckBox, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
								.addGap(6)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addComponent(noMatterRadioButton)
									.addComponent(executiveRadioButton)
									.addComponent(nonExecutiveRadioButton))))
						.addGap(44)))
				.addContainerGap(8, Short.MAX_VALUE))
			.addGroup(gl_panel_1.createSequentialGroup()
				.addContainerGap()
				.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
					.addComponent(outsideQualificationLabel)
					.addComponent(employmentsAcceptedLabel, GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
					.addComponent(lblDunNiveauDe, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGap(51)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addComponent(yesLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lessQualifiedYesRadioButton, GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
						.addComponent(outsideQualificationYesRadioButton, GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)))
				.addGap(34)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addComponent(outsideQualificationNoRadioButton)
					.addComponent(noLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addComponent(lessQualifiedNoRadioButton))
				.addContainerGap(186, Short.MAX_VALUE))
			.addGroup(gl_panel_1.createSequentialGroup()
				.addGap(215)
				.addComponent(yourSearchLabel)
				.addContainerGap(215, Short.MAX_VALUE))
	);
	gl_panel_1.setVerticalGroup(
		gl_panel_1.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_1.createSequentialGroup()
				.addContainerGap()
				.addComponent(yourSearchLabel)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
					.addComponent(searchedEmploymentsLabel)
					.addComponent(searchedEmploymentsTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(15)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
					.addComponent(contractsTypeLabel)
					.addComponent(interimCheckBox)
					.addComponent(searchedStatusLabel)
					.addComponent(executiveRadioButton))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
					.addComponent(cdiCheckBox)
					.addComponent(nonExecutiveRadioButton))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
					.addComponent(cddCheckBox)
					.addComponent(noMatterRadioButton))
				.addGap(3)
				.addComponent(allCheckBox)
				.addGap(29)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
					.addComponent(employmentsAcceptedLabel)
					.addComponent(yesLabel)
					.addComponent(noLabel))
				.addGap(10)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
					.addComponent(outsideQualificationYesRadioButton)
					.addComponent(outsideQualificationLabel)
					.addComponent(outsideQualificationNoRadioButton))
				.addGap(11)
				.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
					.addComponent(lessQualifiedYesRadioButton, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
					.addComponent(lblDunNiveauDe)
					.addComponent(lessQualifiedNoRadioButton))
				.addContainerGap(64, Short.MAX_VALUE))
	);
	
	familyStatus = new ButtonGroup();
	familyStatus.add(aloneRadioButton);
	familyStatus.add(marriedRadioButton);
	familyStatus.add(divorcedRadioButton);
	familyStatus.add(civilUnionRadioButton);
	familyStatus.add(concubineRadioButton);
	familyStatus.add(widowRadioButton);
	
	cashAffiliate = new ButtonGroup();
	cashAffiliate.add(generalSchemeCityRadioButton);
	cashAffiliate.add(msaRadioButton);
	cashAffiliate.add(otherRadioButton);
	
	salaryModalities = new ButtonGroup();
	salaryModalities.add(byCheckRadioButton);
	salaryModalities.add(byTransferRadioButton);
	
	searchedStatus = new ButtonGroup();
	searchedStatus.add(executiveRadioButton);
	searchedStatus.add(nonExecutiveRadioButton);
	searchedStatus.add(noMatterRadioButton);
	
	outsideQualification = new ButtonGroup();
	outsideQualification.add(outsideQualificationYesRadioButton);
	outsideQualification.add(outsideQualificationNoRadioButton);
	
	lessQualified = new ButtonGroup();
	lessQualified.add(lessQualifiedYesRadioButton);
	lessQualified.add(lessQualifiedNoRadioButton);
		panel_1.setLayout(gl_panel_1);
	
	textfields = new JTextField[] {
		lastNameTextField,
		spousesNameTextField,
		firstnameTextField,
		addressTextField,
		postalCodeTextField,
		cityTextField,
		mobilePhoneTextField,
		housePhoneTextField,
		birthPlaceTextField,
		departmentTextField,
		nationalityTextField,
		countryTextField,
		birthdateTextField,
		socialSecurityTextField,
		affiliateCashTextField,
		otherTextField,
		searchedEmploymentsTextField
	};
	
	toggleButtons = new JToggleButton[] {
		aloneRadioButton,
		marriedRadioButton,
		divorcedRadioButton,
		civilUnionRadioButton,
		concubineRadioButton,
		widowRadioButton,
		generalSchemeCityRadioButton,
		msaRadioButton,
		otherRadioButton,
		byCheckRadioButton,
		byTransferRadioButton,
		interimCheckBox,
		cddCheckBox,
		cdiCheckBox,
		allCheckBox,
		executiveRadioButton,
		nonExecutiveRadioButton,
		noMatterRadioButton,
		outsideQualificationYesRadioButton,
		outsideQualificationNoRadioButton,
		lessQualifiedYesRadioButton,
		lessQualifiedNoRadioButton
	};
	
	disableAll();
    }
    
    /**
     * Obtenir les r�ponses texte saisies par le candidat
     * @return Un tableau de chaines de caract�res
     */
    public String[] getStringResponses() {
	String[] responses = new String[textfields.length];
	for (int i = 0, l = textfields.length ; i < l ; i ++) 
	    responses[i] = textfields[i].getText();

	return responses;
    }
    
    /**
     * Effacer tous les champs texte
     */
    public void clearTextFields() {
	for (JTextField jtf : textfields){
	    jtf.setText("");
	}
    }
    
    /**
     * Obtenir les cases coch�es ou non par le candidat
     * @return Un tableau de bool�ens (coch�e ou non)
     */
    public boolean[] getChecked() {
	boolean[] responses = new boolean[toggleButtons.length];
	for (int i = 0, l = toggleButtons.length ; i < l ; i ++)
	    responses[i] = toggleButtons[i].isSelected();

	return responses;
    }
    
    /**
     * D�cocher toutes les cases
     */
    public void uncheckToggles() {
	searchedStatus.clearSelection();
	outsideQualification.clearSelection();
	lessQualified.clearSelection();
	salaryModalities.clearSelection();
	cashAffiliate.clearSelection();
	familyStatus.clearSelection();
	interimCheckBox.setSelected(false);
	cdiCheckBox.setSelected(false);
	cddCheckBox.setSelected(false);
	allCheckBox.setSelected(false);
    }
    
    /**
     * D�sactiver tous les champs textes et les cases
     */
    public void disableAll() {
	for (JTextField jtf : textfields)
	    jtf.setEnabled(false);
	for (JToggleButton jtb : toggleButtons)
	    jtb.setEnabled(false);
    }
    
    /**
     * Activer tous les champs textes et les cases
     */
    public void enableAll() {
	for (JTextField jtf : textfields)
	    jtf.setEnabled(true);
	for (JToggleButton jtb : toggleButtons)
	    jtb.setEnabled(true);
    }
}
