package ui;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import static data.Constants.*;

import java.awt.Container;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Frame principale de l'application
 * @author Rapha�l ALVES
 *
 */
public class MainFrame extends JFrame {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Container contentPane;
    private JMenuBar menu = null;

    public MainFrame() {
	super(MAIN_TITLE);
	BufferedImage image = null;
	try {
	    // le logo en haut a gauche de l'application
	    image = ImageIO.read(getClass().getResource("/icon.png"));
	} catch (IOException e) {
	    e.printStackTrace();
	}
	setIconImage(image);
	try {
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InstantiationException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IllegalAccessException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (UnsupportedLookAndFeelException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setSize(WINDOW_FIRST_WIDTH, WINDOW_FIRST_HEIGHT);
	setVisible(true);
	setLocationRelativeTo(null);
	setResizable(false);
	
	contentPane = getContentPane();
    }
    
    /**
     * Changer le contenu de la frame
     * @param content Le nouveau JPanel � afficher
     */
    public void setContent(JPanel content) {
	contentPane.removeAll();
	contentPane.add(content);
    }
    
    /**
     * Changer le menu de la frame
     * @param menu Le nouveau menu � afficher
     */
    public void setMenu(JMenuBar menu) {
	this.menu = menu;
	setJMenuBar(this.menu);
	setVisible(true);
    }
    
    /**
     * Changer les dimensions de la frame
     * @param width La largeur
     * @param height La hauteur
     */
    public void setDimensions(int width, int height) {
	setSize(width, height);
	setVisible(true);
	setLocationRelativeTo(null);
    }
    
    /**
     * Lorsque l'on clique sur "suivant" alors qu'aucune autre image n'est disponible
     */
    public void noMoreImages() {
	JOptionPane.showMessageDialog(this,
		NOMOREIMAGES_MESSAGE,
		ERROR_TITLE,
		JOptionPane.ERROR_MESSAGE);
    }
}
