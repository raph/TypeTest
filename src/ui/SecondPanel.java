package ui;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.image.BufferedImage;

import listeners.NextListener;
/**
 * Second JPanel � afficher dans la frame, contient le panel avec
 * les images, le panel avec le formulaire, et celui avec les controles
 * @author ar
 *
 */
public class SecondPanel extends JPanel {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private ImagePanel imagesPanel;
    private FormPanel formPanel;
    private ControlsPanel controlsPanel;
    
    private Timer timer;

    public SecondPanel() {
	setBackground(Color.WHITE);
	setLayout(new BorderLayout());

        imagesPanel = new ImagePanel();
        formPanel = new FormPanel();
        controlsPanel = new ControlsPanel();

        JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        sp.setDividerLocation(580);
        sp.setEnabled(false);
        sp.setDividerSize(8);
        
        JSplitPane sp2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        sp2.setDividerLocation(680);
        sp2.setEnabled(false);
        sp2.setDividerSize(8);
        
        sp2.setLeftComponent(imagesPanel);
        sp2.setRightComponent(controlsPanel);

        sp.setLeftComponent(sp2);
        sp.setRightComponent(formPanel);
        add(sp, BorderLayout.CENTER);
        
        controlsPanel.getNextButton().addActionListener(new NextListener(this));
        
        timer = new Timer(controlsPanel);
    }
    
    /**
     * Changer d'image
     * @param image La nouvelle image
     */
    public void displayImage(BufferedImage image) {
	imagesPanel.setBackground(image);
    }
    
    public ControlsPanel getControlsPanel() {
	return controlsPanel;
    }
    
    /**
     * Obtenir le timer
     * @return Le timer
     */
    public Timer getTimer() {
	return timer;
    }
    
    /**
     * Obtenir les r�ponses au format texte
     * @return Les r�ponses au format texte
     */
    public String[] getStringResponses() {
	return formPanel.getStringResponses();
    }
    
    /**
     * Obtenir l'�tat des cases (coch�es/non coch�es) au format bool�en
     * @return Un tableau de bool�ens
     */
    public boolean[] getChecked() {
	return formPanel.getChecked();
    }
    
    /**
     * Vider tous les champs texte
     */
    public void clearTextFields() {
	formPanel.clearTextFields();
    }
    
    /**
     * D�cocher toutes les cases
     */
    public void uncheckToggles() {
	formPanel.uncheckToggles();
    }
    
    /**
     * Afficher le nombre de champs corrects
     * @param n Le nombre de champs corrects
     */
    public void setCorrectsNumber(int n) {
	controlsPanel.setCorrectsNumber(n);
    }
    
    /**
     * Afficher le nombre de champs incorrects
     * @param n Le nombre de champs incorrects
     */
    public void setIncorrectNumber(int n) {
	controlsPanel.setIncorrectNumber(n);
    }
    
    /**
     * D�sactiver tous les champs
     */
    public void disableAll() {
	formPanel.disableAll();
	controlsPanel.disable();
    }
    
    /**
     * Afficher les vittesses brutes et r�elles
     * @param real Vitesse r�elle
     * @param gross Vitesse brute
     */
    public void setSpeeds(double real, double gross) {
	controlsPanel.setRealspeed(real);
	controlsPanel.setGrossspeed(gross);
    }
    
    /**
     * Arreter le timer
     */
    public void stopTimer() {
	timer.stopTimer();
    }
    
    /**
     * Activer tous les champs
     */
    public void enableAll() {
	formPanel.enableAll();
    }

}
