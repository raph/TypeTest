package ui;

/**
 * Classe repr�sentant le timer de l'application, ex�cut� dans un Thread � part
 * @author Rapha�l ALVES
 *
 */
public class Timer extends Thread {
    
    /** Secondes restantes */
    private int seconds;
    
    /** Panneau d'information (pour actualiser l'affichage du timer) */
    private ControlsPanel cp;
    
    /** Si le timer est en route ou non */
    private boolean count;
    
    /** Constructeur */
    public Timer(ControlsPanel cp) {
	this.cp = cp;
	seconds = data.Constants.TIME;
	count = true;
    }
    
    /**
     * D�marrer le timer
     */
    public void run() {
	long now = System.currentTimeMillis();
        while(count) {
            if(System.currentTimeMillis() - now >= 1000) {
                now = System.currentTimeMillis();
                seconds --;
            
                cp.setTime(seconds / 60, seconds % 60);
            }
            if (seconds == 0)
        	count = false;
        }
        // Lorsque le temps s'est �coul� ou que le Chrono est arrett�
        main.Main.app.stop();
    }
    
    /**
     * Arr�ter le timer
     */
    public void stopTimer() {
	count = false;
    }
}
