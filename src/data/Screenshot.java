package data;

import java.awt.Component;
import java.awt.image.BufferedImage;

/**
 * Faire une capture d'�cran du logiciel
 * @author Rapha�l ALVES
 *
 */
public class Screenshot {
    
    public static BufferedImage getScreenShot(Component component) {
	BufferedImage image = new BufferedImage(
		component.getWidth(),
		component.getHeight(),
		BufferedImage.TYPE_INT_RGB
	);
	
	component.paint(image.getGraphics());
	
	return image;
    }

}
