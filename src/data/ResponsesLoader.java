package data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Ici on charge les r�ponses au format texte et cases a cocher
 * @author Rapha�l ALVES
 *
 */
public class ResponsesLoader {
    
    /** Les r�ponses au format texte */
    private ArrayList<String[]> strResponses;
    
    /** Les r�ponses au format booleen (cases � cocher ou non) */
    private ArrayList<boolean[]> boolResponses;
    
    /** D�limiteur des fichiers CSV */
    private static final String DELIMITER = ",";
    
    /** O� l'on en est dans la lecture des r�ponses par le main */
    private int current_index;
    
    public ResponsesLoader() {
	strResponses = new ArrayList<String[]>();
	boolResponses = new ArrayList<boolean[]>();
	current_index = 0;
    }
    
    /**
     * Chargement des r�ponses dans les ArrayList
     */
    public void load() {
	BufferedReader fileReaderStr = null;
	BufferedReader fileReaderBool = null;
	String lineStr = "";
        String lineBool = "";
	try {
            fileReaderStr = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/responses_str.csv")));
            fileReaderBool = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/responses_bool.csv")));
            
            while ((lineStr = fileReaderStr.readLine()) != null)
        	strResponses.add(lineStr.split(DELIMITER, -1));
        	
            while ((lineBool = fileReaderBool.readLine()) != null) {
        	String[] bools = lineBool.split(DELIMITER);
        	boolean[] b = new boolean[bools.length]; 
        	for (int i = 0 ; i < bools.length ; i ++)
        	    b[i] = (bools[i].equals("0")) ? false : true;
        	boolResponses.add(b);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                fileReaderStr.close();
                fileReaderBool.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Passer � l'index suivant
     */
    public void next() {
	current_index ++;
    }
    
    /**
     * Obtenir les r�ponses format texte de l'index courant
     * @return
     */
    public String[] getStrings() {
	return strResponses.get(current_index);
    }
    
    /**
     * Obtenir les r�ponses au format booleen de l'index courant
     * @return
     */
    public boolean[] getBooleans() {
	return boolResponses.get(current_index);
    }
}
