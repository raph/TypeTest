package data;

import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;

import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.IOException;

import static data.Constants.NUMBER_OF_IMAGES;

/**
 * Classe chargeant les images. On le fait dans un thread car l'optimisation
 * de l'image avec la librairie Scalr prend quelques secondes, ainsi les
 * images peuvent s'afficher plus vite puisqu'elles sont d�j� charg�es.
 * @author Rapha�l ALVES
 *
 */
public class Images extends Thread {

    /** Les images */
    private ArrayList<BufferedImage> images;
    
    /** Si le chargement est fini ou pas */
    private boolean loadFinished;
    
    /**
     * Constructeur
     */
    public Images() {
	loadFinished = false;
    }
    
    /**
     * On charge les images et on les optimise avec Scalr
     */
    public void run() {
	images = new ArrayList<BufferedImage>();
	for (int i = 1 ; i <= NUMBER_OF_IMAGES ; i ++) {
	    try {
		images.add(
		    Scalr.resize(
			    ImageIO.read(
				    getClass().getResource("/" + i + ".jpg")
			    ), Method.ULTRA_QUALITY, 2000
		    )
		 );
	    } catch (IllegalArgumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } catch (ImagingOpException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
	loadFinished = true;
    }
    
    /**
     * Obtenir l'image num�ro i
     * @param i
     * @return
     */
    public BufferedImage get(int i) {
	if (loadFinished)
	    return images.get(i - 1);
	else {
	    while (images.size() -1 < i - 1)
		try {
		    sleep(300);
		} catch (InterruptedException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	    
	    return images.get(i - 1);
	}
    }
}
