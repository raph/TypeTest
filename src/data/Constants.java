package data;

/**
 * Toutes les constantes utilisées dans l'application
 * @author Raphaël ALVES
 *
 */
public final class Constants {
    
    /** Les titres */
    public static final String MAIN_TITLE = "Test de saisie",
	    		       ERROR_TITLE = "Erreur";
    
    /** Les labels */
    public static final String LASTNAME_LABEL = "Nom : ",
	    		       FIRSTNAME_LABEL = "Prénom : ",
	    		       ADDRESS_LABEL = "Adresse : ",
	    		       POSTALCODE_LABEL = "CP : ",
	    		       CITY_LABEL = "Ville : ",
	    		       ID_LABEL = "Votre identité",
	    		       SPOUSESNAME_LABEL = "Nom d'épouse : ",
	    		       PHONE_MOBILE_LABEL = "Tel : Mobile : ",
	    		       HOME_LABEL = "Domicile : ",
	    		       BIRTHPLACE_LABEL = "Lieu de naissance : ",
	    		       DEPARTMENT_LABEL = "Département : ",
	    		       NATIONALITY_LABEL = "Nationalité : ",
	    		       COUNTRY_LABEL = "Pays : ",
	    		       BIRTHDATE_LABEL = "Né le : ",
	    		       FAMILYSTATUS_LABEL = "Situation de famille : ",
	    		       SINGLE_LABEL = "Célibataire",
	    		       MARRIED_LABEL = "Marié(e)",
	    		       DIVORCED_LABEL = "Divorcé",
	    		       CIVILUNION_LABEL = "Pacsé(e)",
	    		       COHABITANT_LABEL = "Concubin(e)",
	    		       WIDOWER_LABEL = "Veuf(ve)",
	    		       SOCIALSECURITYNUMBER_LABEL = "Numéro de sécutité sociale : ",
	    		       CASHAFFILIATE_LABEL = "Caisse d'affiliation : ",
	    		       GENERALSCHEMECITY_LABEL = "Régime général/ville : ",
	    		       MSA_LABEL = "MSA",
	    		       OTHER_LABEL = "Autre : ",
	    		       SALARYMODALITIES_LABEL = "Modalités de règlement de vos salaires : ",
	    		       BYCHECK_LABEL = "Par chèque",
	    		       BYTRANSFER_LABEL = "Par virement",
	    		       YOURSEARCH_LABEL = "Votre recherche",
	    		       JOBSSEARCHED_LABEL = "Emploi(s) recherché(s) : ",
	    		       SEARCHEDCONTRACTS_LABEL = "Type(s) de contrat(s) recherché(s) : ",
	    		       INTERIM_LABEL = "Intérim",
	    		       CDI_LABEL = "CDI",
	    		       CDD_LABEL = "CDD",
	    		       SEARCHEDSTATUS_LABEL = "Statut recherché : ",
	    		       EXECUTIVE_LABEL = "Cadre",
	    		       NONEXECUTIVE_LABEL = "Non cadre",
	    		       INDIFFERENT_LABEL = "Indifférent",
	    		       ACCEPTEDJOBS_LABEL = "Acceptez vous des emplois : ",
	    		       YES_LABEL = "Oui",
	    		       NO_LABEL = "Non",
	    		       OUTSIDEQUALIFICATION_LABEL = "En dehors de votre qualification : ",
	    		       LESSQUALIFIEDLEVEL_LABEL = "D'un niveau de qualification inférieur : ",
	    		       CORRECTFIELDS_LABEL = "Nombre de champs corrects : ",
	    		       ERRORSNUMBER_LABEL = "Nombre d'erreurs : ",
	    		       GROSSTYPINGSPEED_LABEL = "Vitesse de frappe brute",
	    		       REALTYPINGSPEED_LABEL = "Vitesse de frappe réelle : ",
	    		       REMAININGTIME_LABEL = "Temps restant : ",
	    		       START_LABEL = "Démarrer",
	    		       CANDIDATE_LABEL = "Candidat : ",
	    		       ALLCONTRACTS_LABEL = "Tous types de contrats",
	    		       NUMBEROFCHILDREN_LABEL = "Nombre d'enfants *",
	    		       SPEED_LABEL = " champs / mn";
    
    /** Les boutons */
    public static final String NEXT_BUTTON = "Suivant",
    			       VALIDATE_BUTTON = "✔ Valider";
    
    /** Les menus */
    public static final String FILE_MENU = "Fichier",
	    		       HELP_MENU = "?";
    
    /** Les sous menus */
    public static final String QUIT_MENU_ITEM = "Quitter",
	    		       ABOUT_MENU_ITEM = "A propos",
	    		       NEW_MENU_ITEM = "Nouveau",
	    		       SAVE_MENU_ITEM = "Enregistrer",
	    		       PRINT_MENU_ITEM = "Imprimer";
    
    /** Message d'erreur */
    public static final String NOMOREIMAGES_MESSAGE = "Aucune autre image disponible.";
    
    /** Message affiché dans "A propos" */
    public static final String ABOUT_MODAL = "Test de saisie version 1.0\n\rDéveloppé par Raphaël ALVES pour le groupe Partnaire\n\r© 2015 Groupe Partnaire";
    
    /** Constantes numériques */
    public static final int WINDOW_FIRST_WIDTH = 350, 
	    		    WINDOW_FIRST_HEIGHT = 400,
	    		    WINDOW_SECOND_WIDTH = 1155,
	    		    WINDOW_SECOND_HEIGHT = 950,
	    		    NUMBER_OF_IMAGES = 8,
	    		    TIME = 300;
}
