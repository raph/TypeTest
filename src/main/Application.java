package main;

import static data.Constants.NUMBER_OF_IMAGES;
import static data.Constants.WINDOW_SECOND_HEIGHT;
import static data.Constants.WINDOW_SECOND_WIDTH;

import java.awt.image.BufferedImage;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.text.Normalizer;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.commons.io.FilenameUtils;

import data.Images;
import data.PrintableScreenshot;
import data.ResponsesLoader;
import data.Screenshot;
import ui.FirstMenu;
import ui.FirstPanel;
import ui.MainFrame;
import ui.SecondMenu;
import ui.SecondPanel;

/**
 * 
 * @author Rapha�l ALVES
 *
 */
public class Application {
    
    private MainFrame window;
    private FirstPanel fp;
    private SecondPanel sp;
    private Images images;
    private ResponsesLoader rl;
    
    private int current = 0;
    private int corrects;
    private int incorrects;
    
    private double grossspeed;
    
    public Application() {
	images = new Images();
    	images.start();
    	rl = new ResponsesLoader();
    	rl.load();
    	corrects = 0;
    	incorrects = 0;
    	grossspeed = 0;
    }
    
    public void run() {
	window = new MainFrame();
	fp = new FirstPanel();
    	window.setContent(fp);
    	window.setMenu(new FirstMenu());
    }
    
    /**
     * Stopper le logiciel lorsque le chrono est fini ou lorsqu'il n'y a plus d'images a afficher.
     */
    public void stop() {
    	sp.disableAll();
    	compareResponsesAtStop();
    	sp.setSpeeds(corrects / 5.0, grossspeed / 5.0);
    }
    
    /**
     * Pour passer d'une image a la suivante
     */
    public void next() {
    	// Si on a d�j� utilis� toutes les images, on stop tout
    	if (current + 1 > NUMBER_OF_IMAGES) {
    		window.noMoreImages();
    		sp.stopTimer();
    	}
    	else {
    		current ++;
	    
    		// Si c'est la premi�re image, cas particulier
    		if (current == 1) {
    			sp = new SecondPanel();
    			sp.getControlsPanel().setFirstname(ucFirst(fp.getFirstname()));
    			sp.getControlsPanel().setLastname(fp.getLastname().toUpperCase());
    			sp.displayImage(images.get(current));
    			window.setContent(sp);
    			window.setMenu(new SecondMenu());
    			window.setDimensions(WINDOW_SECOND_WIDTH, WINDOW_SECOND_HEIGHT);
    		}
    		else {
    			sp.displayImage(images.get(current));
    			sp.clearTextFields();
    			sp.uncheckToggles();
    		}
    	}
    }
    
    /**
     * Comparer les r�ponses de l'utilisateur aux bonnes r�ponses, gestion des scores en fonction
     */
    public void compareResponses() {
    	String[] userStrings = sp.getStringResponses();
    	boolean[] userChecked = sp.getChecked();
	
    	String[] responsesStrings = rl.getStrings();
    	boolean[] responsesChecked = rl.getBooleans();
	
    	String userInput;
    	for (int i = 0, l = userStrings.length ; i < l ; i ++) {
    		// on met en MAJUSCULES et on vire les accents
    		userInput = Normalizer.normalize(userStrings[i].toUpperCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	    
    		// Si l'utilisateur a saisi quelque chose alors que c'�tait un champ vide
    		if (responsesStrings[i].equals("") && ! userInput.equals("")) {
    			incorrects ++;
    			grossspeed ++;
    		}
    		// Si la bonne r�ponse n'est pas un champ vide et que l'utilisateur a �crit la bonne r�ponse
    		else if (! responsesStrings[i].equals("") && userInput.equals(responsesStrings[i])) { 
    			corrects ++;
    			grossspeed ++;
    		}
    		// Si la bonne r�ponse n'est pas un champ vide et que l'utilisateur a saisi une mauvaise r�ponse, mais pas un champ vide
    		else if (! responsesStrings[i].equals("") && ! userInput.equals("") && ! userInput.equals(responsesStrings[i])) {
    			incorrects ++;
    			grossspeed ++;
    		}
    		// Si la bonne r�ponse n'�tais pas un champ vide et que l'utilisateur a saisi une mauvaise r�ponse
    		else if (! responsesStrings[i].equals("")  && ! userInput.equals(responsesStrings[i]))
    			incorrects ++;
    	}
    	
    	for (int i = 0, l = userStrings.length ; i < l ; i ++) {
    		// S'il ne fallait pas cocher la case et qu'elle est coch�e
    		if ((! responsesChecked[i]) && userChecked[i]) {
    			incorrects ++;
    			grossspeed ++;
    		}
    		// S'il fallait cocher la case et qu'elle n'est pas coch�e
    		else if ((! userChecked[i]) && responsesChecked[i]) {
    			incorrects ++;
    		}
    		// Si la case devait �tre coch�e et qu'elle est bien coch�e
    		else if (userChecked[i] && responsesChecked[i]) {
    			corrects ++;
    			grossspeed ++;
    		}
    	}
    	
    	sp.setCorrectsNumber(corrects);
    	sp.setIncorrectNumber(incorrects);
    	if (current != NUMBER_OF_IMAGES)
    		rl.next();
    }
    
    /**
     * Comparer les r�ponses de l'utilisateur au bonnes r�ponses mais cette fois ci
     * lorsque le chrono arrive � la fin. Ici les champs vides ne seront pas compt�s
     * comme des erreurs. Voir la m�thode si dessus pour explication des conditions.
     */
    public void compareResponsesAtStop() {
    	String[] userStrings = sp.getStringResponses();
    	boolean[] userChecked = sp.getChecked();
	
    	String[] responsesStrings = rl.getStrings();
    	boolean[] responsesChecked = rl.getBooleans();
	
    	String userInput;
    	for (int i = 0, l = userStrings.length ; i < l ; i ++) {
    		userInput = userStrings[i].toUpperCase();
    		if (responsesStrings[i].equals("") && ! userInput.equals("")) {
    			incorrects ++;
    			grossspeed ++;
    		}
    		else if (! responsesStrings[i].equals("") && userInput.equals(responsesStrings[i])) { 
    			corrects ++;
    			grossspeed ++;
    		}
    		else if (! responsesStrings[i].equals("") && ! userInput.equals("") && ! userInput.equals(responsesStrings[i])) {
    			incorrects ++;
    			grossspeed ++;
    		}
    	}
	
    	for (int i = 0, l = userStrings.length ; i < l ; i ++) {
    		if ((! responsesChecked[i]) && userChecked[i]) {
    			incorrects ++;
    			grossspeed ++;
    		}
    		else if (userChecked[i] && responsesChecked[i]) {
    			corrects ++;
    			grossspeed ++;
    		}
    	}
    	
    	sp.setCorrectsNumber(corrects);
    	sp.setIncorrectNumber(incorrects);
    	if (current != NUMBER_OF_IMAGES)
    		rl.next();
    }
    
    public void save() {
	BufferedImage img = Screenshot.getScreenShot(sp.getControlsPanel());
	JFileChooser fc = new JFileChooser();
	int returnval = fc.showSaveDialog(null);
	if (returnval == JFileChooser.APPROVE_OPTION) {
	    File file = fc.getSelectedFile();
	    if (! FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("png")) {
		file = new File(file.toString() + ".png");
		file = new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName())+".png");
	    }
	    
	    try {
		ImageIO.write(img, "png", file);
	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
    }
    
    public void print() {
	PrinterJob printJob = PrinterJob.getPrinterJob();
	printJob.setPrintable(new PrintableScreenshot(printJob, Screenshot.getScreenShot(sp.getControlsPanel())));
	if (printJob.printDialog()) {
            try {
                printJob.print();
            } catch (PrinterException prt) {
                prt.printStackTrace();
            }
        }
    }
    
    public void showAbout() {
	JOptionPane.showMessageDialog(window, data.Constants.ABOUT_MODAL);
    }
    
    /**
     * Comme ucfirst en PHP, sert � passer la premi�re lettre d'un mot en majuscules
     * et le reste en minuscules. Utile pour le pr�nom de l'utilisateur.
     * @param str
     * @return
     */
    public String ucFirst(String str) {
	return (str.isEmpty()) ? "" : str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }

}
