package main;

/**
 * Classe d'entr�e du logiciel
 * @author Rapha�l ALVES
 *
 */
public class Main {
    
    public static Application app;
    
    /**
     * Point d'entr� du logiciel
     * @param args
     */
    public static void main(String [] args) {
    	app = new Application();
    	app.run();
    }
}